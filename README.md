## Overview

This Python application organizes the media files (pictures and videos) in directories by date.

This application is written using Python 3.6 and later.

## Motivation

I really like being able to easily organize the pictures and videos from multiple devices (phones, cameras, tablets)
into a single set of folders organized by date.

## How to Run

In the top-level directory:

    $ python run.py

## Configuration

There are (3) key parameters that should be defined in run.py:

| Parameter                       | Description                                                |
| ------------------------------- | ---------------------------------------------------------- |
| SOURCE_DIRECTORY                | Directory containing media files (such as from an iPhone)  |
| DESTINATION_DIRECTORY_PICTURES  | Directory where picture files should be stored             |
| DESTINATION_DIRECTORY_VIDEOS    | Directory where video files should be stored               |

## Unit Testing

    $ pytest
